#!/bin/sh

# ---------------------------
# Anomura
# ---------------------------
# For the script to work correctly, Fedora must be installed using the network installer with the following settings
# Linux kernel: Linux (vanilla)
# Graphics platform: Wayland
# Windows manager: Sway 
# Distribution file system: btrfs
# Sound subsystem: pipewire
# Terminal command shell: bash
# Network: NetworkManager
# Repositories: fedora fedora-modular rpm-fusioan-free rpm-fusioan-nonfree

set -e

notify () {
  echo "---------------------------"
  echo $1
  echo "---------------------------"
}

# ---------------------------
# Installation 
# ---------------------------
notify "Installing and configuring Fedora with the anomura desktop environment"
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y neovim man-db htop alacritty xclip xorg-xhost xorg-xwayland wayland 

# ---------------------------
# Fonts
# ---------------------------
notify "Install fonts"
sudo dnf install -y noto-fonts noto-fonts-emoji ttf-liberation otf-font-awesome
sudo rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm

# ---------------------------
# Sway  
# ---------------------------
notify "Sway and base software"
sudo dnf install -y lightdm sway swaybg wofi swayidle swaylock waybar wl-clipboard xdg-desktop-portal-wlr polkit mc 
sudo ln -sfn $(readlink -f ../../etc/lightdm/lightdm.conf) /etc/lightdm/lightdm.conf
# https://wiki.archlinux.org/title/LightDM#Installation

# ---------------------------
# Grub
# ---------------------------
notify "Modify GRUB options"
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet threadirqs cpufreq.default_governor=performance"/g' /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg

# ---------------------------
# Internet 
# ---------------------------
notify "Programs for Internet surfing"
sudo dnf install -y firefox

# ---------------------------
# Workflow
# ---------------------------
notify "Programs for organization office and workflow"
sudo dnf install -y gsimplecal zathura zathura-djvu zathura-pdf-poppler 

# ---------------------------
# Graphics
# ---------------------------
notify "Programs for working with graphics"
sudo dnf install -y imv grim slurp 

# ---------------------------
# Music
# ---------------------------
notify "Programs for listening to music and controlling volume"
sudo dnf install -y pavucontrol cmus 

# ---------------------------
# Video
# ---------------------------
notify "Video viewers"
sudo dnf install -y mpv

# ---------------------------
# AppImage Installer
# ---------------------------
# notify "Installing third-party software using the AppImage Installer"
# cd ~ && git clone https://gitlab.com/calmiralinux/appimage-installer

# ---------------------------
# Config
# ---------------------------
notify "Copying configs to working directories"
mkdir -pv ~/{Applications,Documents,Downloads,Music,Pictures,Videos}

ln -sfn $(readlink -f ../../usr/share/wallpapers) $(readlink -f ~/Pictures/wallpapers)
# ln -sfn $(readlink -f ../../config/foot) $(readlink -f ~/.config/foot)
ln -sfn $(readlink -f ../../config/sway) $(readlink -f ~/.config/sway)
ln -sfn $(readlink -f ../../config/waybar) $(readlink -f ~/.config/waybar)
ln -sfn $(readlink -f ../../config/wofi) $(readlink -f ~/.config/wofi)

# ---------------------------
# systemd
# ---------------------------
notify "Enable systemd services"
sudo systemctl enable NetworkManager.service
sudo systemctl enable lightdm.service

# ---------------------------
# Uninstallation 
# ---------------------------
# notify "Remove unnecessary programs and cleaning cache"
# sudo dnf autoremove 

# ---------------------------
# FINISHED
# ---------------------------
notify "The script has been completed. Please restart your PC / laptop"
